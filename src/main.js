import { createApp } from 'vue'
import './assets/style/tailwind.css';
import App from './App.vue'

createApp(App).mount('#app')
